import unittest

from ikea import requester

class TestIkeaRequester(unittest.TestCase):
    def setUp(self):
        self._ikea_api = requester.Requester()

    def test_request_single_package_size(self):
        expected_json = {"width": "7 cm", "height": "6 cm", "length": "139 cm", "weight": "3.55 kg"}
        product_url = "https://www.ikea.com/my/en/p/sagstua-bed-frame-black-luroey-s49268912/"
        response = self._ikea_api.request_single_package_size(product_url)

        self.assertEqual(expected_json, response.json)


if __name__ == '__main__':
    unittest.main()
