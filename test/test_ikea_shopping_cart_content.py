import unittest

from ikea import shopping_cart_content

class MckIkeaRequester:
    def request_single_package_size(self, product_url):
        response = Response()
        response.url = product_url
        response.status_code = 200
        response.json = {"width": "7 cm", "height": "6 cm", "length": "139 cm", "weight": "3.55 kg"}
        return response

class Response:
    def __init__(self):
        self.url = None
        self.status_code = None
        self.json = None


class TestShoppingCartContent(unittest.TestCase):
    def setUp(self):
        self._ikea_api = MckIkeaRequester()
        self._shopping_cart_content = shopping_cart_content.ShoppingContent(self._ikea_api)

    def test_api_call(self):
        expected_result = [{"width": "7 cm", "height": "6 cm", "length": "139 cm"}, {"width": "7 cm", "height": "6 cm", "length": "139 cm"}]
        product_url = "https://www.ikea.com/my/en/p/sagstua-bed-frame-black-luroey-s49268912/"
        self._shopping_cart_content.add_product_to_shopping_cart(product_url)
        self._shopping_cart_content.add_product_to_shopping_cart(product_url)
        package_sizes = self._shopping_cart_content.get_size_of_all_packages()

        self.assertEqual(expected_result, package_sizes)


if __name__ == '__main__':
    unittest.main()
