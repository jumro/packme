import requests
from bs4 import BeautifulSoup


class Requester:
    def __init__(self):
        pass


    def request_single_package_size(self, product_url):
        response = Response()
        package_size = {}

        ikea_response = requests.get(product_url)
        response.url = product_url
        response.status_code = ikea_response.status_code

        if response.status_code != 200:
            print("request denied")
            return

        soup = BeautifulSoup(ikea_response.text, "html.parser")
        soup_package_details = soup.find(id="pip_package_details")
        soup = soup_package_details.findAll("span")

        for idx, item in enumerate(soup, 0):
            if not (item and item.string):
                continue
            if item.string == "Width:":
                package_size["width"] = soup[idx+1].get_text()
            if item.string == "Height:":
                package_size["height"] = soup[idx+1].get_text()
            if item.string == "Length:":
                package_size["length"] = soup[idx+1].get_text()
            if item.string == "Diameter:":
                package_size["diameter"] = soup[idx+1].get_text()
            if item.string == "Weight:":
                package_size["weight"] = soup[idx+1].get_text()

        response.json = package_size

        return response


class Response:
    def __init__(self):
        self.url = None
        self.status_code = None
        self.json = None
