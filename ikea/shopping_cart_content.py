
class ShoppingContent():
	def __init__(self, ikea_api):
		self._ikea_api = ikea_api
		self._urls_of_shopping_cart_items = []
		self._all_package_sizes = []

	def get_size_of_all_packages(self):
		for url in self._urls_of_shopping_cart_items:
			response = self._ikea_api.request_single_package_size(url)
			product_size = {"width": response.json["width"],
							"height": response.json["height"],
							"length": response.json["length"]}
			self._all_package_sizes.append(product_size)

		return self._all_package_sizes

	def add_product_to_shopping_cart(self, url):
		self._urls_of_shopping_cart_items.append(url)

	




