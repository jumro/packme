import sys
sys.path.append('/ikea/')

from ikea import shopping_cart_content
from ikea import requester

def main():
    _ikea_api = requester.Requester()
    _shopping_content = shopping_cart_content.ShoppingContent(_ikea_api)

if __name__ == "__main__":
    main()

